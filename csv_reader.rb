require 'csv'
require './table.rb'
require './field.rb'

class Csv_reader
  @file
  @table

  def initialize(class_name, file)
    @file = file
    @table = Table.new
    @table.name = class_name
  end

  def read_csv
    begin
      file = File.new(@file)
      properties = CSV.read(file)

      CSV.foreach(file) do |row|
        field = Field.new
        field.name = row[1]
        field.datatype = row[0]

        @table.add_field(field)
      end

      end
    rescue
      puts "[Error] Cannot read file: #{@file}"
    end

    def get_table
      @table
    end

  end
