class Annotation

  attr_accessor :caption
  attr_accessor :required

  def initialize
    @required = "false"
    @caption = nil
  end

end