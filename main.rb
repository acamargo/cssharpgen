require './generator.rb'
require './csv_reader.rb'
require './java_reader.rb'

type = ARGV[0]
files = ARGV[1]


Dir[files + "/*"].each { |file|
  #puts "Generating code for: #{file}"

  class_name = File.basename file.sub(".java", "")
  reader = Java_reader.new(class_name, file)
  reader.read()
  generator = Generator.new(reader.get_table)

  generator.generate_rest_api(type)

}



