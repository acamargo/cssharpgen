require './annotation.rb'

class Field
  attr_accessor :name
  attr_accessor :datatype
  attr_accessor :value
  attr_accessor :annotation

  def initialize
    @annotation = Annotation.new
  end

  #[ClpColumnAttribute("caID","Tag Name")]

  def build_annotation
    begin
    if (!annotation.caption.nil? & !annotation.required.nil?)
      return "[ClpColumnAttribute(\"" + annotation.caption + "\", " + annotation.required + ")]"
    end

    return "[ClpColumnAttribute]"
    rescue
      puts "incomplete attributes!"
      return ""
    end

  end

end
