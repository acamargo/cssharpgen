class Table
  attr_accessor :name
  attr_accessor :table_annotation
  @list_fields

  def initialize
    @list_fields = Array.new
  end

  def add_field(field)
      @list_fields << field
  end

  def print_fields
    @list_fields.each { |field| puts field.name }
  end

  def get_fields
    @list_fields
  end

  def construct_annotation()
    #  [ClpTableAttribute("tCNF_Medidas")]
    begin
      return "[ClpTableAttribute(\"" + table_annotation + "\")]"
    rescue
      return ""
    end

  end

end
