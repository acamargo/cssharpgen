require 'erb'

class Generator
  @table

  def initialize(table)
    @table = table
  end

  def generate_entities(type)
    fields = @table.get_fields
    class_name = @table.name
    table_annotation = @table.construct_annotation()

    renderer = ERB.new(File.new('templates/class_template').read, nil, '%')
    puts "Generating csharp entity class #{@table.name}..."
    self.write_file("#{@table.name}.cs", renderer.result(binding), type)
  end

  def generate_service_interfaces(type)
    class_name = @table.name

    renderer = ERB.new(File.new('templates/interface_template').read, nil, '%')
    puts "Generating csharp service interface class #{@table.name}..."
    self.write_file("I#{@table.name}Service.cs", renderer.result(binding), type)
  end

  def generate_service_implementations(type)
    class_name = @table.name

    renderer = ERB.new(File.new('templates/implementation_template').read, nil, '%')
    puts "Generating csharp service implementation class #{@table.name}..."
    self.write_file("#{@table.name}Service.cs", renderer.result(binding), type)
  end

  def generate_rest_api(type)
    class_name = @table.name

    begin

      case type
        when "Dau"
          path = class_name.downcase
        when "Iec"
            if (class_name.include? "Iec870510")
               path = class_name.downcase
            elsif class_name.include? type
              path = class_name.sub(type, "").downcase
            end

            if class_name.downcase.include? "efacec"
              path = class_name.downcase.sub "efacec", ""
            end

            if (path.nil?)
              path = class_name.downcase
            end

          when "Iec61850"
            path = class_name.downcase.sub("61850", "")
          when "Insum"
            path = class_name.sub(type, "").downcase
          when "Jbus"
            path = class_name.sub(type, "").downcase
          when "Lw"
            path = class_name.sub(type, "").downcase
          when "ModbusTcp"
            if (class_name.include? "ModbusTcp")
              path = class_name.sub("ModbusTcp", "").downcase
            else
              path = class_name.sub("Modbus", "").downcase
            end
          when "Sie"
            path = class_name.sub(type, "").downcase
          when "Silcom"
            path = class_name.sub(type, "").downcase
          when "Spa"
            path = class_name.sub(type, "").downcase
          when "Tag"
            path = class_name.sub("Efacec", "").downcase
          else
            "You gave me #{type} -- I have no idea what to do with that."

      end

      renderer = ERB.new(File.new('templates/rest_template').read, nil, '%')
      puts "Generating csharp REST API class #{@table.name}..."
      self.write_file("#{@table.name}Controller.cs", renderer.result(binding), type)

    rescue
      puts "[ERROR] cannot generate class: #{class_name}"

    end
  end

  # write file
  def write_file(filename, contents, folder)
    File.open('generation/' + folder + "/" + filename, 'w') { |f| f << contents }
  end

end
