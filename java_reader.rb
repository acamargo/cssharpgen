require './table.rb'

class Java_reader
  @table
  @file

  @annotation

  def initialize(class_name, file)
    @table = Table.new
    @table.name = class_name
    @file = file
    @annotation = Annotation.new
  end

  def read()
    sw = false

    File.open(@file, "r") do |f|
      f.each_line do |line|

       unless (/(\s)*@ClpTable(.)*/.match(line).nil?)
        process_table_annotation(line)
       end

       unless (/(\s)*@ClpColumn(.)*/.match(line).nil?)
        process_caption(line)
        sw = true
       end
       unless (/(\s)*private(\s)+[a-zA-Z]+(\s)+[a-zA-Z0-9]+(\s)*=(\s)*.+;/.match(line).nil?)
         if(sw == true)
           #process_value_init(line)
           process_value(line)
           sw = false
         end
       end
       unless (/(\s)*private(\s)+[a-zA-Z]+(\s)+[a-zA-Z0-9]+(\s)*;/.match(line).nil?)
         if(sw == true)
           process_value(line)
           sw = false
         end
       end

        unless (/(\s)*@NotNull(.)*/.match(line).nil?)
          process_not_null(line)
        end

      end
    end

  end

  def process_value(line)
    values = line.split(' ')

     case values[1]
       when "Float" || "float"
         values[1] = "float"
       when "Integer" || "int"
         values[1] = "int"
       when "Double" || "double"
         values[1] = "double"
       when "Long" || "long"
         values[1] = "long"
       when "Boolean" || "boolean"
         values[1] = "Boolean"
       when "String"
         values[1] = "string"
       else
         values[1] = "string"
     end

    field = Field.new

    str = values[2].sub(";", "")
    field.name = str
    field.datatype = values[1]

    field.annotation = @annotation
    @annotation = Annotation.new
    @table.add_field(field)

  end

  def process_value_init(line)
    values = line.split(' ')

    case values[1]
      when "Float" || "float"
        values[1] = "float"
      when "Integer" || "int"
        values[1] = "int"
      when "Double" || "double"
        values[1] = "double"
      when "Long" || "long"
        values[1] = "long"
      when "Boolean" || "boolean"
        values[1] = "Boolean"
      when "String"
        values[1] = "string"
      else
        values[1] = "string"
    end

    field = Field.new

    str = values[2].sub(";", "")
    field.name = str
    field.datatype = values[1]

    init = line.split('=')
    field.value = init[1]

    field.annotation = @annotation
    @annotation = Annotation.new
    @table.add_field(field)


  end

  def process_caption(line)
    begin
      caption = line[/caption(\s)*=(\s)*"(.*?)"/]
      str = caption.to_s
      unless (caption.nil?)
        @annotation.caption = (str[/"(.*?)"/]).gsub("\"", "")
      end
    rescue
      puts "wtf?"
    end
  end

  def process_not_null(line)
    @annotation.required = "true"
  end

  def get_table
    @table
  end

  def process_table_annotation(line)
    caption = line[/name(\s)*=(\s)*"(.*?)"/]
    str = caption.to_s
    unless (caption.nil?)
      @table.table_annotation = (str[/"(.*?)"/]).gsub("\"", "")
    end
  end
end