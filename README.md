Ruby code generator to generate the C# code from a Java project (ENSDM).

This includes:
- Entities (model) 
- Service interfaces and implementations 
- REST API interface 